import { FETCH_DATA, ADD_MSG, DEL_MSG, UPD_MSG } from './actionsList';

const initialState = {
  messages: []
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return action.payload;
    case ADD_MSG:
      return [...state, action.payload];
    case DEL_MSG:
      return [...state].filter(message => message.id !== action.payload);
    case UPD_MSG:
      const {id} = action.payload;
      const updatedState = [...state].filter(message => message.id !== id);

      return [...updatedState, action.payload];

    default:
      return state;
  }
};
