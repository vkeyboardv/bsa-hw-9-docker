import { FETCH_DATA, ADD_MSG, DEL_MSG, UPD_MSG } from './actionsList';

export const fetchData = () => async dispatch => {
  const res = await fetch("https://api.myjson.com/bins/1hiqin")
    .then(response => response.json());

  dispatch({
    type: FETCH_DATA,
    payload: res
  });
};

export const addMsg = (newMsg) => {
  return {
    type: ADD_MSG,
    payload: newMsg
  }
}

export const deleteMsg = (delMsgId) => {
  return {
    type: DEL_MSG,
    payload: delMsgId
  }
}

export const updateMsg = (updMsg) => {
  return {
    type: UPD_MSG,
    payload: updMsg
  }
}
