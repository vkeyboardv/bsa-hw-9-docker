import React from 'react';
import Message from './Message';

class MessageList extends React.Component {

  getMessages() {
    const messagesArray = this.props.messages.map(
      message => <Message message={message} key={message.id} deleteMsg={this.props.deleteMsg} updateMsg={this.props.updateMsg} />);

    return messagesArray;
  }

  render() {
    return (
      <div className="main-chat-messages">
        <div className="messages">
          {this.getMessages()}
        </div>
      </div>);
  }
}

export default MessageList;
