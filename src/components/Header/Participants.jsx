import React from 'react';

class Participants extends React.Component {
  render() {
    return (<div className="main-chat-participants-counter">
      Participants: <span>{this.props.uniqueUsers}</span>
    </div>);
  }
}

export default Participants;
