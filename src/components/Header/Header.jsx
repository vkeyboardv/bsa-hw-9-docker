import React from 'react';
import Participants from './Participants';
import MessagesCounter from './MessagesCounter';
import LastMessageDate from './LastMessageDate';

import lodash from 'lodash';

class Header extends React.Component {
  render() {
    const messagesLength = this.props.messages.length;
    const uniqueUsers = lodash.uniqBy(this.props.messages, 'user').length;

    if (messagesLength && uniqueUsers) {
      return (
        <div className="main-chat-header">
          <div className="main-chat-name">
            <h2>Chat Room</h2>
          </div>
          <div className="main-chat-participants">
            <Participants uniqueUsers={uniqueUsers} />
          </div>
          <div className="main-chat-header-messages">
            <MessagesCounter messagesLength={messagesLength} />
          </div>
          <div className="main-chat-last-message-date">
            <LastMessageDate messages={this.props.messages} />
          </div>
        </div>
      );
    }
  }
}

export default Header;
