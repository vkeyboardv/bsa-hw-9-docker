import React from 'react';
import Chat from './containers/Chat/Chat';
import logo from './logo.svg';

// import './App.css';
import './Chat.css';

import { Provider } from 'react-redux';
import { rootReducer } from './store/reducer'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const store = createStore(rootReducer, applyMiddleware(thunk));

class App extends React.Component {
  render() {
    return (<div className="App">
      <div className="App-wrapper">
        <img src={logo} className="App-logo" alt="logo" />
        <Provider store={store}>
          <Chat />
        </Provider>
      </div>
    </div>)
  }
}

export default App;
