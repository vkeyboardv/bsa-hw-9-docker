import React from 'react';
import Header from '../../components/Header/Header';
import MessageList from '../../components/MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import SpinnerElement from '../../components/SpinnerElement';

import { connect } from 'react-redux';
import { fetchData, addMsg, deleteMsg, updateMsg } from '../../store/actions';

class Chat extends React.Component {
  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    if (this.props.messages.length) {
      return (
        <div className="main-chat">
          <Header messages={this.props.messages} />
          <MessageList messages={this.props.messages} deleteMsg={(msgObj) => this.props.deleteMsg(msgObj)} updateMsg={(msgObj) => this.props.updateMsg(msgObj)} />
          <MessageInput messages={this.props.messages} addMsg={(msgObj) => this.props.addMsg(msgObj)} />
        </div>
      );
    } else {
      return (
        <div className="spinner">
          <SpinnerElement />
        </div>
      );
    }
  }
}

const mapStateToProps = state => { return { messages: state } };

export default connect(mapStateToProps, { fetchData, addMsg, deleteMsg, updateMsg })(Chat);
